package edu.week4.http;

import java.net.*;
import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Tools {
    // Data member variables
    private String siteURL;
    private String fileName;
    private String siteHTML;

    // Member methods
    Tools() {
        // set some default values just in case
        this.fileName = "default";
        this.siteURL = "https://www.churchofjesuschrist.org/";
        this.siteHTML= "";
    }
    public String getSiteURL() {
        return this.siteURL;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setSiteURL(String siteURL) {
        this.siteURL = siteURL;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSiteURLInput() {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input and invalid url
            try {
                System.out.println("Please enter a site URL such as 'https://www.churchofjesuschrist.org/':");

                newInput = input.nextLine();

                // Make new URL object with given URL
                URL url = new URL(newInput);
                // test the connection to catch any initial errors and perform error handling
                HttpURLConnection http = (HttpURLConnection) url.openConnection();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be text, symobols or numbers in the form of a website URL\n");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string due to the error
                continueInput = true;
            }
            catch (Exception e) {
                System.out.println("Error: please enter a valid URL.");
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }

    public String getFileNameInput() {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter a name for the file: ");

                newInput = input.nextLine();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be letters and numbers\n");
                // clear input
                input.nextLine();
                // make sure the loop will continue for the current string due to the error
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }


    public void getHtml(String givenURL) {
        // I based this method on what brother Tucketts instruction video showed. The overall structure is from his work
        // but i have modified it some for my program. I couldnt see a way to do what he showed without following it
        // closely. I made a point to add comments to show my understanding of what is happening.

        // we want to make sure we can handle any error so try/catch
        try {
            // Make new URL object with given URL
            URL url = new URL(givenURL);
            // open the http connection
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            // Create a Buffer Reader and bring an input stream into the buffer reader for reading the site html
            BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()));
            // a stringbuilder to put all the strings together.
            StringBuilder sb = new StringBuilder();

            // a string to house each new line of html from the site
            String newline = null;
            // loop through the site html lines till there is a null
            while ((newline= br.readLine()) != null) {
                // add each newline to the string builder and go to the next line
                sb.append(newline + "\n");
            }
            // fill the entire stringbuilder contents into the data member variable
            this.siteHTML = sb.toString();

        } catch (Exception e) {
            System.out.println("Error: Something went wrong connecting to the site. " +
                    "Make sure you entered a valid site.");
        }

    }

    public void saveToFile() {
        File save;

        // make sure that we copied something over from the site. Otherwise end.
        if (this.siteHTML.isEmpty() || this.siteHTML.length() < 1) {
            System.out.println("Error: No HTML was retrieved from the site " + this.siteURL + ".");
            return;
        }

        java.io.PrintWriter output = null;
        try{

            // create a new file and open it. Or open existing file.
            save = new java.io.File(getFileName() + ".html");
            output = new java.io.PrintWriter(new FileWriter(save, true));

            // save the data to the file
            output.print( this.siteHTML);

            // Confirmation (confirms we made it this far)
            System.out.println("The HTML was successfully saved to the file: " + getFileName() + ".html.");
        }
        catch (IOException ex) {
            System.out.println("Error: Something went wrong while saving the html to the file.\n");
        }
        finally {
            // always close the file
            output.close();
        }

    }
}
