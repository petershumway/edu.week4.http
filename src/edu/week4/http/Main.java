package edu.week4.http;


import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        //Introduction message and instructions
        System.out.println("This program allows you to save a copy of a webpage in a html file which can be viewed");
        System.out.println("in a browser.");
        System.out.println("You will need to provide the website and the name of the file you wish to save. Keep in");
        System.out.println("mind using the same file name more than once will overwrite any existing file of that name.");
        System.out.println();

        Tools tools = new Tools();
        String go = "y";

        // a loop to allow multiple uses before exiting the program
        do {
            //prompt user for website & save the site URL string
            tools.setSiteURL(tools.getSiteURLInput());

            //prompt user for the filename & save the string
            tools.setFileName(tools.getFileNameInput());

            //get the html from the site
            tools.getHtml(tools.getSiteURL());

            //save to a file
            tools.saveToFile();

            //go again or done?
            Scanner input = new Scanner(System.in);
            System.out.println();
            System.out.println("Would you like to save the HTML to another site? ");
            go = input.nextLine();

        } while (go.charAt(0) == 'y' || go.charAt(0) == 'Y');

        //goodbye
        System.out.println("Thank you for using this program!");
        System.out.println("Goodbye");
    }
}
